package org.uma.jmetal.util.fileoutput;

import org.uma.jmetal.solution.Solution;

import java.util.List;

/**
 * Created by kathe on 24/3/2017.
 */
public class CoefDeCorrelacion {

    private double[][] matrixCoefRel;
    List<? extends Solution<?>> solutionList;

    public CoefDeCorrelacion(List<? extends Solution<?>> solutionList){
        this.solutionList = solutionList;
        int numberOfObjectives = solutionList.get(0).getNumberOfObjectives();
        matrixCoefRel = new double[numberOfObjectives][numberOfObjectives];
        for(int i=0; i<numberOfObjectives;i++){
            matrixCoefRel[i][i] = 1;
            for(int j=i+1; j<numberOfObjectives;j++){
                matrixCoefRel[i][j] = getDistance(i,j);
                matrixCoefRel[j][i] = matrixCoefRel[i][j];
            }
        }
    }

    public double getDistance(int x, int y) {
        double distance = 0;
        double dividendo = 0;
        double divisor = 0;
        double promX = 0;
        double promY = 0;
        double sumDivX = 0;
        double sumDivY = 0;

        for (int i = 0; i < solutionList.size(); i++) {
            promX += solutionList.get(i).getObjective(x);
            promY += solutionList.get(i).getObjective(y);
        }

        promX = promX / solutionList.size();;
        promY = promY / solutionList.size();

        for (int i = 0; i < solutionList.size(); i++) {
            dividendo += ((solutionList.get(i).getObjective(x) - promX) * (solutionList.get(i).getObjective(y) - promY));
            sumDivX += Math.pow(solutionList.get(i).getObjective(x) - promX, 2);
            sumDivY += Math.pow(solutionList.get(i).getObjective(y) - promY, 2);
        }

        divisor = Math.sqrt(sumDivX * sumDivY);
        distance = dividendo / divisor;

        return distance;
    }

    public double[][] getMatrixCoefRel() {
        return matrixCoefRel;
    }

    public void setMatrixCoefRel(double[][] matrixCoefRel) {
        this.matrixCoefRel = matrixCoefRel;
    }
}
