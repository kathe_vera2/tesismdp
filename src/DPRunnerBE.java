import org.uma.jmetal.problem.IntegerProblem;
import org.uma.jmetal.runner.AbstractAlgorithmRunner;
import org.uma.jmetal.solution.IntegerSolution;
import org.uma.jmetal.util.JMetalLogger;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by kathe on 24/3/2017.
 */
public class DPRunnerBE {
        private IntegerProblem problem;
        private List<IntegerSolution> population;
        private double[][] matrixDistance;
        private int N;
        private int cntObj;
        private int M;
        private boolean isMADecision = false;

        public DPRunnerBE(double[][] mD, int N, int M, boolean isMADecision) {
            this.matrixDistance = mD;
            this.N = N;
            this.M = M;
            this.isMADecision = isMADecision;
            this.cntObj = 5;
        }

        public void run() throws IOException {

            long initTime = System.currentTimeMillis();

            population = new ArrayList();
            int mMax = M;

            //M is a Decision Variable
            if(this.isMADecision){
                mMax = (int) Math.ceil((double)N/2)+1;
                if(mMax<3){
                    mMax=3;
                }
                problem = new MultiobjectiveDP(MultiobjectiveDP.MODPVariant.BE, mMax, N, 0, cntObj, matrixDistance);

            } else {
                problem = new MultiobjectiveDP(MultiobjectiveDP.MODPVariant.BE, M, N, 1, cntObj, matrixDistance);
            }

            List<String> orig = new ArrayList<>();
            for (int i = 1; i <= N; i++) {
                orig.add(i + "");
            }

            combinations(mMax, orig, isMADecision);
            Collections.sort(population, new IntegerSolutionComparator());

            long elapsedTime = System.currentTimeMillis() - initTime;
            JMetalLogger.logger.info("Tiempo Transcurrido (en ms): " + elapsedTime);

            if(this.isMADecision){
                AbstractAlgorithmRunner.printFinalSolutionSetMVariable(population, "BE-M-VAR");
            }else{
                AbstractAlgorithmRunner.printFinalSolutionSet(population, "BE-M");
            }


        }

        private void combinations(int nMax, List<String> orig, boolean isMVariable){

            int initialValue = 3;

            if(!isMVariable)
                initialValue=nMax;

            for(int i=initialValue ; i<=nMax ; i++) {
                IteradorCombinacion it = new IteradorCombinacion(orig, i);
                Iterator s = it.iterator();

                while (s.hasNext()) {
                    List<String> l = (List<String>) s.next();
                    IntegerSolution solution = new DefaultIntegerSolution(problem);

                    for(int j=0 ; j<l.size() ; j++) {
                        solution.setVariableValue(l.size()-j-1, Integer.parseInt(l.get(j)));
                    }

                    for(int j=l.size() ; j<nMax ; j++) {
                        solution.setVariableValue(j, 0);
                    }

                    problem.evaluate(solution);
                    updateND(solution);
                }
            }
        }

        public void updateND(IntegerSolution ind) {
            //Agrega al CP las soluciones NO dominadas
            if (isND(ind) && !contains(ind)) {
                population.add(ind);
            }

            //Elimina del CP las soluciones que pudieron quedar dominadas por ind
            Iterator<IntegerSolution> it = population.iterator();
            while (it.hasNext()) {
                IntegerSolution indTmp = it.next();
                if (paretoDominance(ind, indTmp) > 0) {
                    it.remove();
                }
            }
        }

    /**
     * Verifica si la solucion ind2 es no dominada
     * @param ind
     * @return
     */
    public boolean isND(IntegerSolution ind) {

            boolean retValue = true;

            for (int i = 0; i < population.size(); i++) {
                IntegerSolution indTemp = population.get(i);
                int pd = paretoDominance(ind, indTemp);
                // pd debe ser positivo en todos los casos para que ind2 sea no dominada
                if (pd < 0) {
                    retValue = false;
                    break;
                }
            }
            return retValue;
        }

    /**
     * Retorna true en caso que la solucion ind1 ya exista en la poblacion
     * de lo contrario retorna false
     * @param ind1
     * @return
     */
    public boolean contains(IntegerSolution ind1) {
            boolean retValue = false;

            out:
            for (int i = 0; i < population.size(); i++) {
                IntegerSolution ind2 = population.get(i);
                for (int j = 0; j < ind1.getNumberOfObjectives(); j++) {
                    double d1 = ind1.getObjective(j);
                    double d2 = ind2.getObjective(j);
                    if (d1 != d2) {
                        continue out;
                    }
                }
                retValue = true;
            }

            return retValue;
        }

        /**
         * En caso de maximizacion:
         * > 0 ==> ind1 domina ind2
         * < 0 ==> ind1 es dominado por ind2
         * = 0 ==> ind1 e ind2 son NO comparables
         */
        public static int paretoDominance(IntegerSolution ind1, IntegerSolution ind2) {

            int retValue = 0;
            boolean negativo = false;
            boolean positivo = false;
            boolean igual = false;
            double[] substractions = new double[ind1.getNumberOfObjectives()];

            for (int i = 0; i < ind1.getNumberOfObjectives(); i++) {
                substractions[i] = ind1.getObjective(i) - ind2.getObjective(i);
            }

            for (int i = 0; i < ind1.getNumberOfObjectives(); i++) {
                if (substractions[i] < 0) {
                    negativo = true;
                } else if (substractions[i] > 0) {
                    positivo = true;
                } else {
                    igual = true;
                }
            }

            if (negativo && !positivo && !igual) {
                // < 0 ==> ind2 domina a ind1
                retValue = -1;
            } else if (!negativo && positivo && !igual) {
                // > 0 ==> ind1 domina a ind2
                retValue = 1;
            } else {
                // = 0 ==> ind1 e ind2 son NO comparables
                retValue = 0;
            }

            return retValue;
        }
}
