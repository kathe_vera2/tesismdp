/**
 * Created by kathe on 8/3/2017.
 */
import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.algorithm.multiobjective.nsgaii.NSGAIIBuilder;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.operator.impl.crossover.IntegerSBXCrossover;
import org.uma.jmetal.operator.impl.mutation.IntegerPolynomialMutation;
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.runner.AbstractAlgorithmRunner;
import org.uma.jmetal.solution.IntegerSolution;
import org.uma.jmetal.util.AlgorithmRunner;
import org.uma.jmetal.util.JMetalLogger;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class for configuring and running the NSGA-II algorithm (integer encoding)
 *
 * @author Antonio J. Nebro <antonio@lcc.uma.es>
 */
public class DPRunnerNSGAII extends AbstractAlgorithmRunner {

    private double[][] matrixDistance;
    private int N = 4;
    private int cntObj = 5;
    private int M = 3;

    public DPRunnerNSGAII(double[][] mD, int N, int M) {
        this.matrixDistance = mD;
        this.N = N;
        this.cntObj = 5;
        this.M = M;
    }

    public void run() throws IOException {
        List<IntegerSolution> finalPopulationMGiven = new ArrayList<IntegerSolution>();
        Problem<IntegerSolution> problemMGiven;
        Algorithm<List<IntegerSolution>> algorithmMGiven;
        MutationOperator<IntegerSolution> mutationMGiven;

        List<IntegerSolution> finalPopulationMisADecision = new ArrayList<IntegerSolution>();
        Problem<IntegerSolution> problemMisADecision;
        Algorithm<List<IntegerSolution>> algorithmMisADecision;
        MutationOperator<IntegerSolution> mutationMisADecision;

        CrossoverOperator<IntegerSolution> crossover;
        SelectionOperator<List<IntegerSolution>, IntegerSolution> selection;

        //CROSSOVER
        double crossoverProbability = 0.9;
        double crossoverDistributionIndex = 20.0;
        double mutationDistributionIndex = 20.0;
        crossover = new IntegerSBXCrossover(crossoverProbability, crossoverDistributionIndex);
        selection = new BinaryTournamentSelection<IntegerSolution>();

        //M Given
        problemMGiven = new MultiobjectiveDP(MultiobjectiveDP.MODPVariant.NSGAII, M, N, 1,cntObj,matrixDistance);
        double mutationProbabilityMGiven = 1.0 / problemMGiven.getNumberOfVariables();
        mutationMGiven = new IntegerPolynomialMutation(mutationProbabilityMGiven, mutationDistributionIndex);

        algorithmMGiven = new NSGAIIBuilder<IntegerSolution>(problemMGiven, crossover, mutationMGiven)
                .setSelectionOperator(selection)
                .setMaxEvaluations(25000)
                .setPopulationSize(500)
                .build();

        AlgorithmRunner algorithmRunnerMGiven = new AlgorithmRunner.Executor(algorithmMGiven)
                .execute();

        List<IntegerSolution> populationMGiven = algorithmMGiven.getResult();
        for (IntegerSolution integerSolution : populationMGiven) {
            for (int k = 0; k < cntObj; k++) {
                integerSolution.setObjective(k, integerSolution.getObjective(k) * (-1));
            }
        }

        for (IntegerSolution integerSolution : populationMGiven) {
            if (!contains(finalPopulationMGiven,integerSolution)) {
                finalPopulationMGiven.add(integerSolution);
            }
        }

        Collections.sort(finalPopulationMGiven, new IntegerSolutionComparator());

        long computingTime = algorithmRunnerMGiven.getComputingTime();

        JMetalLogger.logger.info("Total execution time for M given: " + computingTime + "ms");
        printFinalSolutionSet(finalPopulationMGiven, "NSGA-M");

        //printQualityIndicators(finalPopulationMGiven,"E:\\PC\\FP-UNA\\Tesis - Disimilitudes\\materiales-NSGAII\\jMetal-master\\BE-M");

        //M is a Decision Variable
        int mMax = (int) Math.ceil((double)N/2)+1;
        //int mMax = N-1;
        if(mMax<3){
            mMax=3;
        }
        problemMisADecision = new MultiobjectiveDP(MultiobjectiveDP.MODPVariant.NSGAII, mMax, N, 0,cntObj,matrixDistance);
        double mutationProbabilityMisADecision = 1.0 / problemMisADecision.getNumberOfVariables();
        mutationMisADecision = new IntegerPolynomialMutation(mutationProbabilityMisADecision, mutationDistributionIndex);

        algorithmMisADecision = new NSGAIIBuilder<IntegerSolution>(problemMisADecision, crossover, mutationMisADecision)
                .setSelectionOperator(selection)
                .setMaxEvaluations(25000)
                .setPopulationSize(500)
                .build();

        AlgorithmRunner algorithmRunnerMisADecision = new AlgorithmRunner.Executor(algorithmMisADecision)
                .execute();

        List<IntegerSolution> populationMisADecision = algorithmMisADecision.getResult();
        for (IntegerSolution integerSolution : populationMisADecision) {
            for (int k = 0; k < cntObj; k++) {
                integerSolution.setObjective(k, integerSolution.getObjective(k) * (-1));
            }
        }

        for (IntegerSolution integerSolution : populationMisADecision) {
            if (!contains(finalPopulationMisADecision,integerSolution)) {
                finalPopulationMisADecision.add(integerSolution);
            }
        }

        Collections.sort(finalPopulationMisADecision, new IntegerSolutionComparator());

        computingTime = algorithmRunnerMisADecision.getComputingTime();

        JMetalLogger.logger.info("Total execution time for M as a decision variable: " + computingTime + "ms");
        printFinalSolutionSetMVariable(finalPopulationMisADecision,"NSGA-M-VAR");

        //printQualityIndicators(finalPopulationMisADecision,"E:\\PC\\FP-UNA\\Tesis - Disimilitudes\\materiales-NSGAII\\jMetal-master\\BE-M-VAR");

    }

    public boolean contains(List<IntegerSolution> finalPopulation, IntegerSolution ind1) {
        boolean retValue = false;

        out:
        for (int i = 0; i < finalPopulation.size(); i++) {
            IntegerSolution ind2 = finalPopulation.get(i);
            for (int j = 0; j < ind1.getNumberOfObjectives(); j++) {
                double d1 = ind1.getObjective(j);
                double d2 = ind2.getObjective(j);
                if (d1 != d2) {
                    continue out;
                }
            }
            retValue = true;
        }

        return retValue;
    }
}

