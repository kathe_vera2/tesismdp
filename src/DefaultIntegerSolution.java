/**
 * Created by kathe on 8/3/2017.
 */

import org.uma.jmetal.problem.IntegerProblem;
import org.uma.jmetal.solution.IntegerSolution;

import java.util.*;

import org.uma.jmetal.solution.impl.AbstractGenericSolution;

/**
 * Defines an implementation of an integer solution
 *
 * @author Antonio J. Nebro <antonio@lcc.uma.es>
 */
@SuppressWarnings("serial")
public class DefaultIntegerSolution
        extends AbstractGenericSolution<Integer, IntegerProblem>
        implements IntegerSolution {


    /**
     * Constructor
     */
    public DefaultIntegerSolution(IntegerProblem problem) {
        super(problem);

        repairDuplicateSolution();
        initializeObjectiveValues();
    }

    /**
     * Copy constructor
     */
    public DefaultIntegerSolution(DefaultIntegerSolution solution) {
        super(solution.problem);
        repairDuplicateSolution();
        for (int i = 0; i < problem.getNumberOfVariables(); i++) {
            setVariableValue(i, solution.getVariableValue(i));
        }

        for (int i = 0; i < problem.getNumberOfObjectives(); i++) {
            setObjective(i, solution.getObjective(i));
        }

        // overallConstraintViolationDegree = solution.overallConstraintViolationDegree ;
        // numberOfViolatedConstraints = solution.numberOfViolatedConstraints ;
        attributes = new HashMap<Object, Object>(solution.attributes);
    }

    @Override
    public Integer getUpperBound(int index) {
        return problem.getUpperBound(index);
    }

    @Override
    public Integer getLowerBound(int index) {
        return problem.getLowerBound(index);
    }

    @Override
    public DefaultIntegerSolution copy() {
        return new DefaultIntegerSolution(this);
    }

    @Override
    public String getVariableValueString(int index) {
        return getVariableValue(index).toString();
    }

    private void repairDuplicateSolution() {
        int Mmax = (int) Math.ceil((double) problem.getUpperBound(0) / 2);
        int Mmin = 3;
        if(Mmax < Mmin)
            Mmax = Mmin;
        int zeroMaxCount = Mmax-Mmin;
        boolean isMADecision = problem.getLowerBound(0).equals(0);
        int zeroCount = 0;
        Set<Integer> generated = new HashSet<Integer>();
        for (int i = 0; i < problem.getNumberOfVariables(); i++) {
            Integer v = getVariableValue(i);
            if(v != null && !v.equals(0))
                generated.add(v);
            if (v != null && v.equals(0))
                zeroCount++;
        }

        if(zeroCount > (zeroMaxCount))
            zeroCount = zeroMaxCount;

        int max = problem.getNumberOfVariables();
        if(isMADecision)
            max -= zeroCount;
        while (generated.size() < max) {
            Integer next = randomGenerator.nextInt(1, getUpperBound(0));
            // As we're adding to a set, this will automatically do a containment check
            generated.add(next);
        }

        int i = 0;
        for (Integer a : generated) {
            if (a != null)
                setVariableValue(i, a);
            i++;
        }
        while (i < problem.getNumberOfVariables()){
            setVariableValue(i, 0);
            i++;
        }
    }
    //System.out.println("DefaultIntegerSolution2.initializeIntegerVariables(). " + this);
}

