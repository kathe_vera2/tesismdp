/**
 * Created by kathe on 8/3/2017.
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

public class Distance {
    private int N;
    private int M;
    private double[][] matrixDistance;

    /**
     * Carga las distancias en una matriz en memoria
     */
    public double[][] loadMatrix( String fileName) throws IOException {

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line = br.readLine();
            String[] st = line.split(" ");
            N = new Integer(st[0]);
            M = new Integer(st[1]);
            matrixDistance = new double[N][N];
            while ((line = br.readLine()) != null) {
                st = line.split(" ");
                matrixDistance[Integer.parseInt(st[0])][Integer.parseInt(st[1])] = Double.parseDouble(st[2]);
                matrixDistance[Integer.parseInt(st[1])][Integer.parseInt(st[0])] = Double.parseDouble(st[2]);
            }
        }
        return  matrixDistance;
    }

    public int getN() {
        return N;
    }

    public void setN(int n) {
        N = n;
    }

    public int getM() {
        return M;
    }

    public void setM(int m) {
        M = m;
    }

    public double[][] getMatrixDistance() {
        return matrixDistance;
    }

    public void setMatrixDistance(double[][] matrixDistance) {
        this.matrixDistance = matrixDistance;
    }
}

