import org.uma.jmetal.solution.IntegerSolution;

import java.util.Comparator;

/**
 * Created by kathe on 24/3/2017.
 */
public class IntegerSolutionComparator implements Comparator<IntegerSolution> {

    @Override
    public int compare(IntegerSolution sol1, IntegerSolution sol2) {
        int c;
        c = Double.compare(sol1.getObjective(0), sol2.getObjective(0));
        if(c==0)
            c = Double.compare(sol1.getObjective(1), sol2.getObjective(1));
        if(c==0)
            c = Double.compare(sol1.getObjective(2), sol2.getObjective(2));
        if(c==0)
            c = Double.compare(sol1.getObjective(3), sol2.getObjective(3));
        if(c==0)
            c = Double.compare(sol1.getObjective(4), sol2.getObjective(4));
        return c;
    }

}
