/**
 * Created by kathe on 8/3/2017.
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Clase que orquestara el circuito completo de los algoritmos BE y NSGAII
 * para obtener el frente pareto de los M mas disimiles.
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {

        String textFile = "E:\\PC\\FP-UNA\\Tesis - Disimilitudes\\instances\\MDP\\GKD-a\\GKD-a_2_n10_m2.txt";
        if (args.length == 1) {
            textFile = args[0];
        }

        Distance d = new Distance();
        double[][] dist = d.loadMatrix(textFile);

        DPRunnerBE runnerMGiven = new DPRunnerBE(dist, d.getN(), d.getM(),false);
        runnerMGiven.run();

        DPRunnerBE runnerMisADecision = new DPRunnerBE(dist, d.getN(), d.getM(),true);
        runnerMisADecision.run();

        DPRunnerNSGAII runner = new DPRunnerNSGAII(dist, d.getN(), d.getM());
        runner.run();
    }

}
