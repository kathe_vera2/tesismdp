/**
 * Created by kathe on 8/3/2017.
 */
import org.uma.jmetal.util.JMetalException;

import java.io.*;
import java.util.*;

import org.uma.jmetal.problem.impl.AbstractIntegerProblem;
import org.uma.jmetal.solution.IntegerSolution;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Class representing a bi-objective DP (Dispersion Problem) problem. It accepts
 * data files from OPTSICOM: http://www.optsicom.es/
 */
@SuppressWarnings("serial")
public class MultiobjectiveDP extends AbstractIntegerProblem {

    public enum MODPVariant {
        NSGAII, BE
    }
    private MultiobjectiveDP.MODPVariant variant;
    protected double[][] distanceMatrix;
    protected boolean isMaDecisionVariable;
    private final double MAX_NUMBER = 10000000000000000000000000.0;
    private final double MIN_NUMBER = -10000000000000000000000000.0;

    /**
     * Creates a new MultiobjectiveDP problem instance
     */
    public MultiobjectiveDP(MultiobjectiveDP.MODPVariant v, Integer M, Integer N,Integer lowerNumberVariable, Integer numberOfObjectives, double[][] matrixDistance) throws IOException {
        setNumberOfVariables(M);
        setNumberOfObjectives(numberOfObjectives);
        setName("MultiObjectiveDP");
        this.variant = v;

        distanceMatrix = matrixDistance;

        List<Integer> lowerLimit = new ArrayList<>(getNumberOfVariables());
        List<Integer> upperLimit = new ArrayList<>(getNumberOfVariables());

        for (int i = 0; i < getNumberOfVariables(); i++) {
            lowerLimit.add(lowerNumberVariable);
            upperLimit.add(N);
        }
        isMaDecisionVariable = false;
        if(lowerNumberVariable.equals(0)){
            isMaDecisionVariable = true;
        }

        setLowerLimit(lowerLimit);
        setUpperLimit(upperLimit);
    }

    //SUM
    public Double hallarSum(List<String> listDist) {
        double suma = 0.0;
        for(String dist : listDist){
            String[] elem = dist.split("-");
            int i = Integer.parseInt(elem[0]);
            int j = Integer.parseInt(elem[1]);
            suma+=distanceMatrix[i-1][j-1];
        }
        return suma;
    }

    //MIN
    private Double hallarMin(List<String> listDist) {
        Double min = MAX_NUMBER;
        for(String dist : listDist){
            String[] elem = dist.split("-");
            int i = Integer.parseInt(elem[0]);
            int j = Integer.parseInt(elem[1]);

            if(min > distanceMatrix[i-1][j-1]) {
                min = distanceMatrix[i - 1][j - 1];
                if(min == 0){
                    System.out.println(i + "-" + j +" = " + distanceMatrix[i - 1][j - 1]);
                }
            }


        }
        return min;
    }

    //MIN-SUM
    private Double hallarMinSum(List<String> strings, List<String> listDist) {
        Map<String,Double> minSum = new LinkedHashMap<>();
        for (String key : strings){
            minSum.put(key,0.0);
        }
        for (String distancia : listDist){
            String[] elem = distancia.split("-");
            int i = Integer.parseInt(elem[0]);
            int j = Integer.parseInt(elem[1]);

            double distValue=distanceMatrix[i-1][j-1];

            minSum.put(elem[0], minSum.get(elem[0])+distValue);
            minSum.put(elem[1], minSum.get(elem[1])+distValue);
        }
        double min = MAX_NUMBER;
        for(String key : strings){
            if(minSum.get(key) < min){
                min = minSum.get(key);
            }
        }
        return min;
    }

    //MAX-SUM - calculo para el Diff
    private Double hallarMaxSum(List<String> strings, List<String> listDist) {
        Map<String,Double> minSum = new LinkedHashMap<>();
        for (String key : strings){
            minSum.put(key,0.0);
        }
        for (String distancia : listDist){
            String[] elem = distancia.split("-");
            int i = Integer.parseInt(elem[0]);
            int j = Integer.parseInt(elem[1]);

            double distValue=distanceMatrix[i-1][j-1];

            minSum.put(elem[0], minSum.get(elem[0])+distValue);
            minSum.put(elem[1], minSum.get(elem[1])+distValue);
        }
        double max = MIN_NUMBER;
        for(String key : strings){
            if(minSum.get(key) > max){
                max = minSum.get(key);
            }
        }
        return max;
    }

    //P-Center
    private Double hallarMaxMin(List<String> inM) {
        Map<Integer, Double> minDeLetras = new LinkedHashMap<Integer, Double>();

        List<String> inN = new ArrayList<>();
        for(int i=1; i<=getUpperBound(0);i++){
            inN.add(""+i);
        }

        for(String a : inN){
            minDeLetras.put(Integer.parseInt(a),MAX_NUMBER);
        }


        for(int i=1;i<=distanceMatrix.length;i++){
            for(int j=1;j<=distanceMatrix.length;j++){
                double distValue = distanceMatrix[i-1][j-1];
                if(i <j) {
                    if (inN.contains(String.valueOf(i)) && inM.contains(String.valueOf(j)))
                        minDeLetras.put(i, (minDeLetras.get(i) >= distValue ? distValue : minDeLetras.get(i)));
                    if (inN.contains(String.valueOf(j)) && inM.contains(String.valueOf(i)))
                        minDeLetras.put(j, (minDeLetras.get(j) >= distValue ? distValue : minDeLetras.get(j)));
                }
            }
        }

        double max = MIN_NUMBER;
        for(String a : inN){
            if(minDeLetras.get(Integer.parseInt(a))>max){
                max = minDeLetras.get(Integer.parseInt(a));
            }
        }
        return max;
    }

    /**
     * Evaluate() method dm(M)
     */
    public void evaluate(IntegerSolution solution) {
        double[] fitness = new double[getNumberOfObjectives()];

        List<String> sol = new ArrayList<>();
        int M=0;
        for (int i=0; i<solution.getNumberOfVariables();i++){
            if(solution.getVariableValue(i) != 0){
                M++;
                sol.add(""+solution.getVariableValue(i));
            }
        }
        IteradorCombinacion it2 = new IteradorCombinacion(sol, 2);
        Iterator s2 = it2.iterator();
        List<String> listDist = new ArrayList<>();
        while (s2.hasNext()) {

            List<String> l = (List<String>) s2.next();
            String newA = new String();
            for (int j = l.size() - 1; j >= 0; j--) {
                newA += l.get(j) + "-";
            }
            listDist.add(newA);
        }

        fitness[0] = hallarSum(listDist);
        fitness[1] = hallarMin(listDist);
        fitness[2] = hallarMinSum(sol,listDist);
        fitness[3] = (-1)*(hallarMaxSum(sol,listDist)-hallarMinSum(sol,listDist)); // Diff
        fitness[4] = (-1)*hallarMaxMin(sol);

        if (isMaDecisionVariable){
            for (int k = 0; k < getNumberOfObjectives(); k++) {
                fitness[k] = fitness[k] / M;
            }
        }

        if (this.variant.equals(MODPVariant.BE)) {
            for (int k = 0; k < getNumberOfObjectives(); k++) {
                solution.setObjective(k, round(fitness[k], 2));
            }
        } else if (this.variant.equals(MODPVariant.NSGAII)) {
            for (int k = 0; k < getNumberOfObjectives(); k++) {
                solution.setObjective(k, (-1) * round(fitness[k], 2));
            }
        }
    }

    public double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    @Override
    public IntegerSolution createSolution() {
        //Crear clase propia para implementar el metodo de de inicializar variables controlando
        //el duplicado
        return new DefaultIntegerSolution(this);
    }
}

